# Gerenciador de Arquivos

Gerenciador de arquivos é um projeto referente a um desafio proposto pela Soluti.
O projeto é referente a um sistema de gestão de arquivos, onse de se tem as entidades grupos, usuários e arquivos

Para começar, altere o nome do arquivo doctrine.local.dist.php que está dentro do diretório backend-apigility/config/autoload para doctrine.local.php.

# Obtendo dependencias do backend:

Para iniciar o projeto, devemos obter as dependências nessárias para o backend.
Pelo terminal, acesse o diretório backend-apigility e digite o seguinte comando:

```
composer install
```

Observação: Durante a instalação, será solicitado escolher em qual arquivo serão armazenadas as configurações do módulo ZF\Apigility\Doctrine\Admin. Escolha a opção 2 e posteriormente responda y.

# Obtendo dependencias do front end e construindo a aplicação:

Para obter as dependencias do frontend devemos acessar o diretório do frontend e executar o seguinte comando:

```
npm install
```

Desse modo, devemos construir o projeto caso se tenha alterações no frontend. Assim, execute o seguinte comando:

```
npm run build
```

# Construindo os containers e executando a aplicação

Para criar os containers da aplicação, volte a raiz do projeto e execute o seguinte comando:

```
docker-compose up -d --build
```

Nesta etapa, são gerados alguns arquivos no diretório /backend-apigility/data que precisamos ter acesso para a execução. Desse modo, precisamos dar a permissão para acesso da pasta com o seguinte comando:

```
chmod -R 777 backend-apigility/data/
```

Finalmente, ao se acessar os links a seguir temos acesso as aplicações frontend e backend do projeto.

```
http://localhost:80 : Frontend
http://localhost:81 : Backend

```