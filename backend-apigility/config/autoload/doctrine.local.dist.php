<?php
return [
    'doctrine' => [
        'connection' => [
            'orm_default' => [
                'driverClass' => 'Doctrine\DBAL\Driver\PDOMySql\Driver',
                'params' => [
                    'host'     => '10.10.10.8',
                    'port'     => '3306',
                    'user'     => 'root',
                    'password' => 'root123',
                    'dbname'   => 'gerenciador-de-arquivos',
                    'charset' => 'utf8',
                    'driverOptions' => ['SET NAMES utf8']
                ]
            ]
        ]
    ]
];