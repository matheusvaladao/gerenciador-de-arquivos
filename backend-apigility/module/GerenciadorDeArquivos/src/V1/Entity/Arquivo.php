<?php

namespace GerenciadorDeArquivos\V1\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Arquivos
 *
 * @ORM\Table(name="arquivos", indexes={@ORM\Index(name="fk_arquivos_usuarios1_idx", columns={"usuario_cadastro"})})
 * @ORM\Entity
 */
class Arquivo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=45, nullable=false)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="arquivo", type="string", length=255, nullable=false)
     */
    private $arquivo;

    /**
     * @var string
     *
     * @ORM\Column(name="tamanho", type="string", length=45, nullable=false)
     */
    private $tamanho;

    /**
     * @var \GerenciadorDeArquivos\V1\Entity\Usuario
     *
     * @ORM\ManyToOne(targetEntity="GerenciadorDeArquivos\V1\Entity\Usuario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_cadastro", referencedColumnName="id")
     * })
     */
    private $usuarioCadastro;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="GerenciadorDeArquivos\V1\Entity\Usuario", mappedBy="arquivos")
     */
    private $usuarios;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->usuarios = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nome.
     *
     * @param string $nome
     *
     * @return Arquivo
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome.
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set arquivo.
     *
     * @param string $arquivo
     *
     * @return Arquivo
     */
    public function setArquivo($arquivo)
    {
        $this->arquivo = $arquivo;

        return $this;
    }

    /**
     * Get arquivo.
     *
     * @return string
     */
    public function getArquivo()
    {
        return $this->arquivo;
    }

    /**
     * Set tamanho.
     *
     * @param string $tamanho
     *
     * @return Arquivo
     */
    public function setTamanho($tamanho)
    {
        $this->tamanho = $tamanho;

        return $this;
    }

    /**
     * Get tamanho.
     *
     * @return string
     */
    public function getTamanho()
    {
        return $this->tamanho;
    }

    /**
     * Set usuarioCadastro.
     *
     * @param \GerenciadorDeArquivos\V1\Entity\Usuario|null $usuarioCadastro
     *
     * @return Arquivo
     */
    public function setUsuarioCadastro(\GerenciadorDeArquivos\V1\Entity\Usuario $usuarioCadastro = null)
    {
        $this->usuarioCadastro = $usuarioCadastro;

        return $this;
    }

    /**
     * Get usuarioCadastro.
     *
     * @return \GerenciadorDeArquivos\V1\Entity\Usuario|null
     */
    public function getUsuarioCadastro()
    {
        return $this->usuarioCadastro;
    }

    /**
     * Add usuario.
     *
     * @param \GerenciadorDeArquivos\V1\Entity\Usuario $usuario
     *
     * @return Arquivo
     */
    public function addUsuario(\GerenciadorDeArquivos\V1\Entity\Usuario $usuario)
    {
        $usuario->addArquivo($this);
        $this->usuarios[] = $usuario;

        return $this;
    }

    /**
     * Remove usuario.
     *
     * @param \GerenciadorDeArquivos\V1\Entity\Usuario $usuario
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeUsuario(\GerenciadorDeArquivos\V1\Entity\Usuario $usuario)
    {
        return $this->usuarios->removeElement($usuario);
    }

    /**
     * Get usuario.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsuarios()
    {
        $usuarios = [];

        foreach($this->usuarios as $usuario) {
            $usuarios[] = [
                'id' => $usuario->getId(),
                'nome' => $usuario->getNome()
            ];
        }

        return $usuarios;

    }

    public function removerUsuariosVinculados()
    {
        $this->usuarios = new \Doctrine\Common\Collections\ArrayCollection();
    }
}
