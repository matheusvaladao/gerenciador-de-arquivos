<?php

namespace GerenciadorDeArquivos\V1\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Usuarios
 *
 * @ORM\Table(name="usuarios", uniqueConstraints={@ORM\UniqueConstraint(name="email_UNIQUE", columns={"email"})}, indexes={@ORM\Index(name="fk_usuarios_grupos1_idx", columns={"grupo"})})
 * @ORM\Entity
 */
class Usuario
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=80, nullable=false)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=50, nullable=false)
     */
    private $email;

    /**
     * @var \GerenciadorDeArquivos\V1\Entity\Grupo
     *
     * @ORM\ManyToOne(targetEntity="GerenciadorDeArquivos\V1\Entity\Grupo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="grupo", referencedColumnName="id")
     * })
     */
    private $grupo;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="GerenciadorDeArquivos\V1\Entity\Arquivo", inversedBy="usuarios")
     * @ORM\JoinTable(name="usuarios_arquivos",
     *   joinColumns={
     *     @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="arquivo_id", referencedColumnName="id")
     *   }
     * )
     */
    private $arquivos;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->arquivos = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nome.
     *
     * @param string $nome
     *
     * @return Usuario
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome.
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return Usuario
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set grupo.
     *
     * @param \GerenciadorDeArquivos\V1\Entity\Grupo|null $grupo
     *
     * @return Usuario
     */
    public function setGrupo(\GerenciadorDeArquivos\V1\Entity\Grupo $grupo = null)
    {
        $this->grupo = $grupo;

        return $this;
    }

    /**
     * Get grupo.
     *
     * @return \GerenciadorDeArquivos\V1\Entity\Grupo|null
     */
    public function getGrupo()
    {
        return $this->grupo;
    }

    /**
     * Add arquivo.
     *
     * @param \GerenciadorDeArquivos\V1\Entity\Arquivo $arquivo
     *
     * @return Usuario
     */
    public function addArquivo(\GerenciadorDeArquivos\V1\Entity\Arquivo $arquivo)
    {
        $this->arquivos[] = $arquivo;

        return $this;
    }

    /**
     * Remove arquivo.
     *
     * @param \GerenciadorDeArquivos\V1\Entity\Arquivo $arquivo
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeArquivo(\GerenciadorDeArquivos\V1\Entity\Arquivo $arquivo)
    {
        return $this->arquivos->removeElement($arquivo);
    }

    /**
     * Get arquivo.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArquivos()
    {
        $arquivos = [];

        foreach($this->arquivos as $arquivo) {
            $arquivos[] = [
                'id' => $arquivo->getId(),
                'nome' => $arquivo->getNome()
            ];
        }

        return $arquivos;

    }
}
