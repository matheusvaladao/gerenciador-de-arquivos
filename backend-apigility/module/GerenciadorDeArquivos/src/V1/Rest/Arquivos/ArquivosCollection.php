<?php
namespace GerenciadorDeArquivos\V1\Rest\Arquivos;

use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\ArrayAdapter;

class ArquivosCollection extends Paginator
{
    public function __construct($arquivosCollection) {
        parent::__construct(new ArrayAdapter($arquivosCollection));
    }
}
