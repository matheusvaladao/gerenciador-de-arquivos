<?php

namespace GerenciadorDeArquivos\V1\Rest\Arquivos;

use GerenciadorDeArquivos\V1\Entity\Grupo;
use GerenciadorDeArquivos\V1\Rest\Usuarios\UsuariosCollection;
use Zend\Paginator\Adapter\ArrayAdapter;
use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;
use GerenciadorDeArquivos\V1\Entity\Arquivo;
use GerenciadorDeArquivos\V1\Entity\Usuario;

class ArquivosResource extends AbstractResourceListener
{

    private $entityManager;

    public function __construct($entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Adicionar um arquivo.
     *
     * @param mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {

        $usuarioCadastro = $this->entityManager->find(Usuario::class, $data->usuarioCadastro);

        if (!$usuarioCadastro) {
            return new ApiProblem(404, 'Usuário de cadastro não encontrado.');
        }

        $inputFilter = $this->getInputFilter();
        $pdfTmp = $inputFilter->getValue('arquivo');

        $arquivo = new Arquivo();
        $arquivo->setNome($pdfTmp['name']);
        $arquivo->setArquivo($pdfTmp['tmp_name']);
        $arquivo->setTamanho($pdfTmp['size']);
        $arquivo->setUsuarioCadastro($usuarioCadastro);

        foreach ($data->usuarios as $idUsuario) {

            $usuarioVinculado = $this->entityManager->find(Usuario::class, $idUsuario);

            if (!$usuarioVinculado) {
                return new ApiProblem(404, 'Usuário vinculado não encontrado.');
            }

            $arquivo->addUsuario($usuarioVinculado);

        }

        $this->entityManager->persist($arquivo);
        $this->entityManager->flush();

        return $arquivo;
    }

    /**
     * Excluir um arquivo.
     *
     * @param mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        $arquivo = $this->entityManager->find(Arquivo::class, $id);

        if (!$arquivo) {
            return new ApiProblem(404, 'Arquivo não encontrado.');
        }

        $caminhoArquivo = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . $arquivo->getArquivo();

        $this->entityManager->remove($arquivo);
        $this->entityManager->flush();

        unlink($caminhoArquivo);

        return new ApiProblem(200, 'Arquivo excluído com sucesso.');
    }

    /**
     * Listar apenas um arquivo.
     *
     * @param mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        $arquivo = $this->entityManager->find(Arquivo::class, $id);

        if (!$arquivo) {
            return new ApiProblem(405, 'Arquivo não encontrado.');
        }

        return $arquivo;

    }

    /**
     * Listar todos os arquivos.
     *
     * @param array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = [])
    {
        return new ArquivosCollection(
            $this->entityManager->getRepository(Arquivo::class)
                ->findAll()
        );
    }

}
