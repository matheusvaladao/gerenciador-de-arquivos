<?php
namespace GerenciadorDeArquivos\V1\Rest\Arquivos;

class ArquivosResourceFactory
{
    public function __invoke($services)
    {
        $entityManager = $services->get('Doctrine\ORM\EntityManager');
        return new ArquivosResource($entityManager);
    }
}
