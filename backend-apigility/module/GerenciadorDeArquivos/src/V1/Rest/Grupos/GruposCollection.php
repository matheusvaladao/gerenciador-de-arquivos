<?php
namespace GerenciadorDeArquivos\V1\Rest\Grupos;

use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\ArrayAdapter;

class GruposCollection extends Paginator
{
    public function __construct($gruposCollection) {
        parent::__construct(new ArrayAdapter($gruposCollection));
    }
}
