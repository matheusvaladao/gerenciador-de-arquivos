<?php
namespace GerenciadorDeArquivos\V1\Rest\Grupos;

class GruposResourceFactory
{
    public function __invoke($services)
    {
        $entityManager = $services->get('Doctrine\ORM\EntityManager');
        return new GruposResource($entityManager);
    }
}
