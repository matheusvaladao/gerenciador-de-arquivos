<?php

namespace GerenciadorDeArquivos\V1\Rest\Grupos;

use GerenciadorDeArquivos\V1\Entity\Usuario;
use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;
use GerenciadorDeArquivos\V1\Entity\Grupo;

class GruposResource extends AbstractResourceListener
{

    private $entityManager;

    public function __construct($entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Adicionar um grupo.
     *
     * @param mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
        $grupo = new Grupo();

        $grupo->setNome($data->nome);

        $this->entityManager->persist($grupo);
        $this->entityManager->flush();

        return $grupo;
    }

    /**
     * Excluir um grupo.
     *
     * @param mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        $grupo = $this->entityManager->find(Grupo::class, $id);

        if (!$grupo) {
            return new ApiProblem(404, 'Grupo não encontrado.');
        }

        $qb = $this->entityManager->createQueryBuilder();
        $qb->select('u')
            ->from( Usuario::class, "u")
            ->where('u.grupo = ?1')
            ->setParameter( 1, $id );
        $usuariosGrupo = $qb->getQuery()->getResult();

        if ($usuariosGrupo) {
            return new ApiProblem(400, 'O grupo não pode ser excluído. Existem usuários vinculados a esse grupo.');
        }

        $this->entityManager->remove($grupo);
        $this->entityManager->flush();

        return new ApiProblem(200, 'Grupo excluído com sucesso.');
    }

    /**
     * Listar apenas um grupo.
     *
     * @param mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        $grupo = $this->entityManager->find(Grupo::class, $id);

        if (!$grupo) {
            return new ApiProblem(404, 'Grupo não encontrado.');
        }

        return $grupo;
    }

    /**
     * Listar todos os grupos.
     *
     * @param array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = [])
    {
        return new GruposCollection(
            $this->entityManager->getRepository(Grupo::class)
                ->findAll()
        );
    }

    /**
     * Alterar um grupo.
     *
     * @param mixed $id
     * @param mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        $grupo = $this->entityManager->find(Grupo::class, $id);

        if (!$grupo) {
            return new ApiProblem(404, 'Grupo não encontrado.');
        }

        $grupo->setNome($data->nome);

        $this->entityManager->persist($grupo);
        $this->entityManager->flush();

        return $grupo;
    }
}
