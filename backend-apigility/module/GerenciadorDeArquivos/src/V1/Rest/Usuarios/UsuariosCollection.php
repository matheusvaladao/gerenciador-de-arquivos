<?php
namespace GerenciadorDeArquivos\V1\Rest\Usuarios;

use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\ArrayAdapter;

class UsuariosCollection extends Paginator
{
    public function __construct($usuariosCollection) {
        parent::__construct(new ArrayAdapter($usuariosCollection));
    }
}
