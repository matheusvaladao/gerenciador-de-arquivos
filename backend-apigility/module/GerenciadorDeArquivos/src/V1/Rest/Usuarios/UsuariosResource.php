<?php
namespace GerenciadorDeArquivos\V1\Rest\Usuarios;

use GerenciadorDeArquivos\V1\Entity\Arquivo;
use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;
use GerenciadorDeArquivos\V1\Entity\Usuario;
use GerenciadorDeArquivos\V1\Entity\Grupo;


class UsuariosResource extends AbstractResourceListener
{
    private $entityManager;

    public function __construct($entityManager) {
        $this->entityManager = $entityManager;
    }

    /**
     * Adicionar um usuário.
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
        $usuario = new Usuario();

        $grupo = $this->entityManager->find(Grupo::class, $data->grupo);

        if (!$grupo){
            return new ApiProblem(404, 'Grupo não encontrado.');
        }

        $usuario->setNome($data->nome);
        $usuario->setEmail($data->email);
        $usuario->setGrupo($grupo);

        $this->entityManager->persist($usuario);
        $this->entityManager->flush();

        return $usuario;
    }

    /**
     * Excluir um usuário.
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        $usuario = $this->entityManager->find(Usuario::class, $id);

        if (!$usuario){
            return new ApiProblem(404, 'Usuário não encontrado.');
        }

        if ($usuario->getArquivos()!=null) {
            return new ApiProblem(400, 'O usuário não pode ser excluído. Existem arquivos vinculados a ele.');
        }

        $this->entityManager->remove($usuario);
        $this->entityManager->flush();

        return new ApiProblem(200,'Usuário excluído com sucesso.');
    }

    /**
     * Listar apenas um usuário.
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        $usuario = $this->entityManager->find(Usuario::class, $id);

        if (!$usuario){
            return new ApiProblem(404, 'Usuário não encontrado.');
        }

        return $usuario;
    }

    /**
     * Listar todos os usuários.
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = [])
    {
        return new UsuariosCollection(
            $this->entityManager->getRepository(Usuario::class)
            ->findAll()
        ); 
    }

    /**
     * Alterar um usuário.
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        
        $usuario = $this->entityManager->find(Usuario::class, $id);

        if (!$usuario){
            return new ApiProblem(404, 'Usuário não encontrado.');
        }

        $grupo = $this->entityManager->find(Grupo::class, $data->grupo);

        if (!$grupo){
            return new ApiProblem(404, 'Grupo não encontrado.');
        }

        $usuario->setNome($data->nome);
        $usuario->setEmail($data->email);
        $usuario->setGrupo($grupo);

        $this->entityManager->persist($usuario);
        $this->entityManager->flush();

        return $usuario;
    }
}
