<?php
namespace GerenciadorDeArquivos\V1\Rest\Usuarios;

class UsuariosResourceFactory
{
    public function __invoke($services)
    {
        $entityManager = $services->get('Doctrine\ORM\EntityManager');
        return new UsuariosResource($entityManager);
    }
}
