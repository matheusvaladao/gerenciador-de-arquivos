<?php
namespace GerenciadorDeArquivos\V1\Rest\ArquivosDownload;

use GerenciadorDeArquivos\V1\Entity\Arquivo;
use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

class ArquivosDownloadResource extends AbstractResourceListener
{

    private $entityManager;

    public function __construct($entityManager) {
        $this->entityManager = $entityManager;
    }

    /**
     * Realizar o download de um arquivo cadastrado no sistema.
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        $arquivo = $this->entityManager->find(Arquivo::class, $id);

        $caminhoArquivo = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.$arquivo->getArquivo();

        if (!file_exists($caminhoArquivo)) {
            return new ApiProblem(405, 'Arquivo não encontrado.');
        }

        header('Content-Type: application/octet-stream');
        header("Content-Transfer-Encoding: Binary");
        header("Content-disposition: attachment; filename=\"".$arquivo->getNome()."\"");
        header('Content-Length: '.filesize($caminhoArquivo));

        readfile($caminhoArquivo);
    }

}
