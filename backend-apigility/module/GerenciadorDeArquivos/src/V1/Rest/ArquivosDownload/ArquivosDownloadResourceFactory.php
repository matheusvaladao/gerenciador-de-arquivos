<?php
namespace GerenciadorDeArquivos\V1\Rest\ArquivosDownload;

use GerenciadorDeArquivos\V1\Rest\Arquivos\ArquivosResource;

class ArquivosDownloadResourceFactory
{
    public function __invoke($services)
    {
        $entityManager = $services->get('Doctrine\ORM\EntityManager');
        return new ArquivosDownloadResource($entityManager);
    }

}
