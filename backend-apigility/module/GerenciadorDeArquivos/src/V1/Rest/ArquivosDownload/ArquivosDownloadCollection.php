<?php
namespace GerenciadorDeArquivos\V1\Rest\ArquivosDownload;

use Zend\Paginator\Adapter\ArrayAdapter;
use Zend\Paginator\Paginator;

class ArquivosDownloadCollection extends Paginator
{
    public function __construct($arquivosDownloadCollection) {
        parent::__construct(new ArrayAdapter($arquivosDownloadCollection));
    }
}
