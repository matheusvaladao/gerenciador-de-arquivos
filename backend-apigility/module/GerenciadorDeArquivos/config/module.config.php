<?php
return [
    'doctrine' => [
        'driver' => [
            'GerenciadorDeArquivosDriver' => [
                'class' => \Doctrine\ORM\Mapping\Driver\AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [
                    0 => './module/GerenciadorDeArquivos/src/V1/Entity',
                ],
            ],
            'orm_default' => [
                'drivers' => [
                    'GerenciadorDeArquivos' => 'GerenciadorDeArquivosDriver',
                ],
            ],
        ],
    ],
    'service_manager' => [
        'factories' => [
            \GerenciadorDeArquivos\V1\Rest\Usuarios\UsuariosResource::class => \GerenciadorDeArquivos\V1\Rest\Usuarios\UsuariosResourceFactory::class,
            \GerenciadorDeArquivos\V1\Rest\Grupos\GruposResource::class => \GerenciadorDeArquivos\V1\Rest\Grupos\GruposResourceFactory::class,
            \GerenciadorDeArquivos\V1\Rest\Arquivos\ArquivosResource::class => \GerenciadorDeArquivos\V1\Rest\Arquivos\ArquivosResourceFactory::class,
            \GerenciadorDeArquivos\V1\Rest\ArquivosDownload\ArquivosDownloadResource::class => \GerenciadorDeArquivos\V1\Rest\ArquivosDownload\ArquivosDownloadResourceFactory::class,
        ],
    ],
    'router' => [
        'routes' => [
            'gerenciador-de-arquivos.rest.usuarios' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/usuarios[/:usuarios_id]',
                    'defaults' => [
                        'controller' => 'GerenciadorDeArquivos\\V1\\Rest\\Usuarios\\Controller',
                    ],
                ],
            ],
            'gerenciador-de-arquivos.rest.grupos' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/grupos[/:grupos_id]',
                    'defaults' => [
                        'controller' => 'GerenciadorDeArquivos\\V1\\Rest\\Grupos\\Controller',
                    ],
                ],
            ],
            'gerenciador-de-arquivos.rest.arquivos' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/arquivos[/:arquivos_id]',
                    'defaults' => [
                        'controller' => 'GerenciadorDeArquivos\\V1\\Rest\\Arquivos\\Controller',
                    ],
                ],
            ],
            'gerenciador-de-arquivos.rest.arquivos-download' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/arquivos/download[/:arquivos_id]',
                    'defaults' => [
                        'controller' => 'GerenciadorDeArquivos\\V1\\Rest\\ArquivosDownload\\Controller',
                    ],
                ],
            ],
        ],
    ],
    'zf-versioning' => [
        'uri' => [
            0 => 'gerenciador-de-arquivos.rest.usuarios',
            1 => 'gerenciador-de-arquivos.rest.grupos',
            2 => 'gerenciador-de-arquivos.rest.arquivos',
            3 => 'gerenciador-de-arquivos.rest.arquivos-download',
        ],
    ],
    'zf-rest' => [
        'GerenciadorDeArquivos\\V1\\Rest\\Usuarios\\Controller' => [
            'listener' => \GerenciadorDeArquivos\V1\Rest\Usuarios\UsuariosResource::class,
            'route_name' => 'gerenciador-de-arquivos.rest.usuarios',
            'route_identifier_name' => 'usuarios_id',
            'collection_name' => 'usuarios',
            'entity_http_methods' => [
                0 => 'GET',
                1 => 'PUT',
                2 => 'DELETE',
                3 => 'PATCH',
                4 => 'POST',
            ],
            'collection_http_methods' => [
                0 => 'GET',
                1 => 'POST',
            ],
            'collection_query_whitelist' => [],
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => \GerenciadorDeArquivos\V1\Entity\Usuario::class,
            'collection_class' => \GerenciadorDeArquivos\V1\Rest\Usuarios\UsuariosCollection::class,
            'service_name' => 'Usuarios',
        ],
        'GerenciadorDeArquivos\\V1\\Rest\\Grupos\\Controller' => [
            'listener' => \GerenciadorDeArquivos\V1\Rest\Grupos\GruposResource::class,
            'route_name' => 'gerenciador-de-arquivos.rest.grupos',
            'route_identifier_name' => 'grupos_id',
            'collection_name' => 'grupos',
            'entity_http_methods' => [
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
                4 => 'POST',
            ],
            'collection_http_methods' => [
                0 => 'GET',
                1 => 'POST',
            ],
            'collection_query_whitelist' => [],
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => \GerenciadorDeArquivos\V1\Entity\Grupo::class,
            'collection_class' => \GerenciadorDeArquivos\V1\Rest\Grupos\GruposCollection::class,
            'service_name' => 'Grupos',
        ],
        'GerenciadorDeArquivos\\V1\\Rest\\Arquivos\\Controller' => [
            'listener' => \GerenciadorDeArquivos\V1\Rest\Arquivos\ArquivosResource::class,
            'route_name' => 'gerenciador-de-arquivos.rest.arquivos',
            'route_identifier_name' => 'arquivos_id',
            'collection_name' => 'arquivos',
            'entity_http_methods' => [
                0 => 'GET',
                1 => 'DELETE',
                2 => 'POST',
            ],
            'collection_http_methods' => [
                0 => 'GET',
                1 => 'POST',
            ],
            'collection_query_whitelist' => [],
            'page_size' => '5',
            'page_size_param' => '5',
            'entity_class' => \GerenciadorDeArquivos\V1\Entity\Arquivo::class,
            'collection_class' => \GerenciadorDeArquivos\V1\Rest\Arquivos\ArquivosCollection::class,
            'service_name' => 'Arquivos',
        ],
        'GerenciadorDeArquivos\\V1\\Rest\\ArquivosDownload\\Controller' => [
            'listener' => \GerenciadorDeArquivos\V1\Rest\ArquivosDownload\ArquivosDownloadResource::class,
            'route_name' => 'gerenciador-de-arquivos.rest.arquivos-download',
            'route_identifier_name' => 'arquivos_id',
            'collection_name' => 'arquivos_download',
            'entity_http_methods' => [
                0 => 'GET',
            ],
            'collection_http_methods' => [
                0 => 'GET',
            ],
            'collection_query_whitelist' => [],
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => \GerenciadorDeArquivos\V1\Entity\Arquivo::class,
            'collection_class' => \GerenciadorDeArquivos\V1\Rest\ArquivosDownload\ArquivosDownloadCollection::class,
            'service_name' => 'ArquivosDownload',
        ],
    ],
    'zf-content-negotiation' => [
        'controllers' => [
            'GerenciadorDeArquivos\\V1\\Rest\\Usuarios\\Controller' => 'HalJson',
            'GerenciadorDeArquivos\\V1\\Rest\\Grupos\\Controller' => 'HalJson',
            'GerenciadorDeArquivos\\V1\\Rest\\Arquivos\\Controller' => 'HalJson',
            'GerenciadorDeArquivos\\V1\\Rest\\ArquivosDownload\\Controller' => 'HalJson',
        ],
        'accept_whitelist' => [
            'GerenciadorDeArquivos\\V1\\Rest\\Usuarios\\Controller' => [
                0 => 'application/vnd.gerenciador-de-arquivos.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ],
            'GerenciadorDeArquivos\\V1\\Rest\\Grupos\\Controller' => [
                0 => 'application/vnd.gerenciador-de-arquivos.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ],
            'GerenciadorDeArquivos\\V1\\Rest\\Arquivos\\Controller' => [
                0 => 'application/vnd.gerenciador-de-arquivos.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ],
            'GerenciadorDeArquivos\\V1\\Rest\\ArquivosDownload\\Controller' => [
                0 => 'application/vnd.gerenciador-de-arquivos.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ],
        ],
        'content_type_whitelist' => [
            'GerenciadorDeArquivos\\V1\\Rest\\Usuarios\\Controller' => [
                0 => 'application/vnd.gerenciador-de-arquivos.v1+json',
                1 => 'application/json',
            ],
            'GerenciadorDeArquivos\\V1\\Rest\\Grupos\\Controller' => [
                0 => 'application/vnd.gerenciador-de-arquivos.v1+json',
                1 => 'application/json',
            ],
            'GerenciadorDeArquivos\\V1\\Rest\\Arquivos\\Controller' => [
                0 => 'application/vnd.gerenciador-de-arquivos.v1+json',
                1 => 'application/json',
                2 => 'multipart/form-data',
            ],
            'GerenciadorDeArquivos\\V1\\Rest\\ArquivosDownload\\Controller' => [
                0 => 'application/vnd.gerenciador-de-arquivos.v1+json',
                1 => 'application/json',
            ],
        ],
    ],
    'zf-hal' => [
        'metadata_map' => [
            'GerenciadorDeArquivos\\V1\\Rest\\Usuarios\\UsuariosEntity' => [
                'entity_identifier_name' => 'id',
                'route_name' => 'gerenciador-de-arquivos.rest.usuarios',
                'route_identifier_name' => 'usuarios_id',
                'hydrator' => \Zend\Hydrator\ArraySerializable::class,
            ],
            \GerenciadorDeArquivos\V1\Rest\Usuarios\UsuariosCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'gerenciador-de-arquivos.rest.usuarios',
                'route_identifier_name' => 'usuarios_id',
                'is_collection' => true,
            ],
            'GerenciadorDeArquivos\\V1\\Rest\\Grupos\\GruposEntity' => [
                'entity_identifier_name' => 'id',
                'route_name' => 'gerenciador-de-arquivos.rest.grupos',
                'route_identifier_name' => 'grupos_id',
                'hydrator' => \Zend\Hydrator\ArraySerializable::class,
            ],
            \GerenciadorDeArquivos\V1\Rest\Grupos\GruposCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'gerenciador-de-arquivos.rest.grupos',
                'route_identifier_name' => 'grupos_id',
                'is_collection' => true,
            ],
            'GerenciadorDeArquivos\\V1\\Rest\\Arquivos\\ArquivosEntity' => [
                'entity_identifier_name' => 'id',
                'route_name' => 'gerenciador-de-arquivos.rest.arquivos',
                'route_identifier_name' => 'arquivos_id',
                'hydrator' => \Zend\Hydrator\ArraySerializable::class,
            ],
            \GerenciadorDeArquivos\V1\Rest\Arquivos\ArquivosCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'gerenciador-de-arquivos.rest.arquivos',
                'route_identifier_name' => 'arquivos_id',
                'is_collection' => true,
            ],
            \GerenciadorDeArquivos\V1\Entity\Usuario::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'gerenciador-de-arquivos.rest.usuarios',
                'route_identifier_name' => 'usuarios_id',
                'hydrator' => \DoctrineModule\Stdlib\Hydrator\DoctrineObject::class,
            ],
            \GerenciadorDeArquivos\V1\Entity\Grupo::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'gerenciador-de-arquivos.rest.grupos',
                'route_identifier_name' => 'grupos_id',
                'hydrator' => \DoctrineModule\Stdlib\Hydrator\DoctrineObject::class,
            ],
            \GerenciadorDeArquivos\V1\Entity\Arquivo::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'gerenciador-de-arquivos.rest.arquivos',
                'route_identifier_name' => 'arquivos_id',
                'hydrator' => \DoctrineModule\Stdlib\Hydrator\DoctrineObject::class,
            ],
            'GerenciadorDeArquivos\\V1\\Rest\\ArquivosDownload\\ArquivosDownloadEntity' => [
                'entity_identifier_name' => 'id',
                'route_name' => 'gerenciador-de-arquivos.rest.arquivos-download',
                'route_identifier_name' => 'arquivos_download_id',
                'hydrator' => \Zend\Hydrator\ArraySerializable::class,
            ],
            \GerenciadorDeArquivos\V1\Rest\ArquivosDownload\ArquivosDownloadCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'gerenciador-de-arquivos.rest.arquivos-download',
                'route_identifier_name' => 'arquivos_id',
                'is_collection' => true,
            ],
        ],
    ],
    'zf-content-validation' => [
        'GerenciadorDeArquivos\\V1\\Rest\\Usuarios\\Controller' => [
            'input_filter' => 'GerenciadorDeArquivos\\V1\\Rest\\Usuarios\\Validator',
        ],
        'GerenciadorDeArquivos\\V1\\Rest\\Grupos\\Controller' => [
            'input_filter' => 'GerenciadorDeArquivos\\V1\\Rest\\Grupos\\Validator',
        ],
        'GerenciadorDeArquivos\\V1\\Rest\\Arquivos\\Controller' => [
            'input_filter' => 'GerenciadorDeArquivos\\V1\\Rest\\Arquivos\\Validator',
        ],
    ],
    'input_filter_specs' => [
        'GerenciadorDeArquivos\\V1\\Rest\\Usuarios\\Validator' => [
            0 => [
                'required' => false,
                'validators' => [],
                'filters' => [],
                'name' => 'id',
            ],
            1 => [
                'required' => true,
                'validators' => [],
                'filters' => [],
                'name' => 'nome',
            ],
            2 => [
                'required' => true,
                'validators' => [],
                'filters' => [],
                'name' => 'email',
            ],
            3 => [
                'required' => true,
                'validators' => [],
                'filters' => [],
                'name' => 'grupo',
            ],
        ],
        'GerenciadorDeArquivos\\V1\\Rest\\Grupos\\Validator' => [
            0 => [
                'required' => false,
                'validators' => [],
                'filters' => [],
                'name' => 'id',
            ],
            1 => [
                'required' => true,
                'validators' => [],
                'filters' => [],
                'name' => 'nome',
            ],
        ],
        'GerenciadorDeArquivos\\V1\\Rest\\Arquivos\\Validator' => [
            0 => [
                'required' => false,
                'validators' => [],
                'filters' => [],
                'name' => 'id',
            ],
            1 => [
                'required' => false,
                'validators' => [
                    0 => [
                        'name' => \Zend\Validator\File\MimeType::class,
                        'options' => [
                            'mimeType' => 'application/pdf',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Zend\Filter\File\RenameUpload::class,
                        'options' => [
                            'randomize' => true,
                            'target' => 'data/arquivos',
                            'use_upload_extension' => true,
                        ],
                    ],
                ],
                'name' => 'arquivo',
                'type' => \Zend\InputFilter\FileInput::class,
            ],
        ],
    ],
    'zf-mvc-auth' => [
        'authorization' => [
            'GerenciadorDeArquivos\\V1\\Rest\\Grupos\\Controller' => [
                'collection' => [
                    'GET' => false,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ],
                'entity' => [
                    'GET' => false,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ],
            ],
        ],
    ],
];
