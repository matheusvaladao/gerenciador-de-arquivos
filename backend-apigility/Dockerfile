#
# Use this dockerfile to run apigility.
#
# Start the server using docker-compose:
#
#   docker-compose build
#   docker-compose up
#
# You can install dependencies via the container:
#
#   docker-compose run apigility composer install
#
# You can manipulate dev mode from the container:
#
#   docker-compose run apigility composer development-enable
#   docker-compose run apigility composer development-disable
#   docker-compose run apigility composer development-status
#
# OR use plain old docker 
#
#   docker build -f Dockerfile-dev -t apigility .
#   docker run -it -p "8080:80" -v $PWD:/var/www apigility
#
FROM php:7.2-apache

RUN apt-get update \
 && apt-get install -y git zlib1g-dev \
 && docker-php-ext-install zip \
 && a2enmod rewrite \
 && sed -i 's!/var/www/html!/var/www/public!g' /etc/apache2/sites-available/000-default.conf \
 && mv /var/www/html /var/www/public \
 && curl -sS https://getcomposer.org/installer \
  | php -- --install-dir=/usr/local/bin --filename=composer \
 && echo "AllowEncodedSlashes On" >> /etc/apache2/apache2.conf

ENV COMPOSER_ALLOW_SUPERUSER 1

RUN apt-get install -fy libfreetype6-dev libjpeg62-turbo-dev libpng-dev \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install gd

RUN apt-get install -fy libicu-dev \
    && docker-php-ext-install intl

RUN apt-get install -fy libxml2-dev \
    && apt-get install -fy libxslt-dev \
    && docker-php-ext-install xsl
    #&& docker-php-ext-install soap \
    #&& docker-php-ext-install xmlrpc \
    #&& docker-php-ext-install wddx

RUN apt-get install -fy libbz2-dev \
    && docker-php-ext-install bz2

RUN docker-php-ext-install zip \
    && docker-php-ext-install pcntl \
    && docker-php-ext-install pdo_mysql \
    && docker-php-ext-install mysqli \
    && docker-php-ext-install exif \
    && docker-php-ext-install bcmath \
    && docker-php-ext-install calendar \
    && docker-php-ext-install gettext \
    && docker-php-ext-install opcache
    #&& docker-php-ext-install sockets \
    #&& docker-php-ext-install shmop \
    #&& docker-php-ext-install sysvmsg \
    #&& docker-php-ext-install sysvsem \
    #&& docker-php-ext-install sysvshm \
    #&& docker-php-ext-install pdo_firebird \
    #&& docker-php-ext-install pdo_dblib \
    #&& docker-php-ext-install pdo_oci \
    #&& docker-php-ext-install pdo_odbc

RUN apt-get update && apt-get install -fy libpq-dev \
    && docker-php-ext-install pdo_pgsql \
    && docker-php-ext-install pgsql

RUN apt-get install -fy curl libcurl4-openssl-dev \
    && docker-php-ext-install curl

RUN apt-get install -fy libedit-dev libreadline-dev \
    && docker-php-ext-install readline

#RUN apt-get install -fy libsnmp-dev \
    #&& apt-get install -fy snmp \
    #&& docker-php-ext-install snmp

#RUN apt-get install -fy libpspell-dev \
    #&& apt-get install -fy aspell-en \
    #&& docker-php-ext-install pspell

#RUN apt-get install -fy librecode0 \
    #&& apt-get install -fy librecode-dev \
    #&& docker-php-ext-install recode

#RUN apt-get install -fy libtidy-dev \
    #&& docker-php-ext-install tidy

RUN apt-get install -fy libgmp-dev \
    && ln -s /usr/include/x86_64-linux-gnu/gmp.h /usr/include/gmp.h \
    && docker-php-ext-install gmp

WORKDIR /var/www