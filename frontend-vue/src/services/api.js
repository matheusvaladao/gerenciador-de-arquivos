import axios from "axios";

const api = axios.create({
  baseURL: "http://localhost:81/",
  timeout: 1000,
  headers: {
    "Content-Type": "application/json;multipart/form-data;"
  }
});

export default api;
