import api from "../services/api";

export default {
  async listar(parametros) {
    return await api.get("arquivos" + parametros);
  },

  async salvar(arquivo) {
    return await api.post("arquivos", arquivo);
  },

  async alterar(arquivo) {
    var novoArquivo = {
      usuarios: arquivo.usuarios
    };

    return await api.put("arquivos/" + arquivo.id, novoArquivo);
  },

  async excluir(arquivo) {
    return await api.delete("arquivos/" + arquivo.id);
  }
};
