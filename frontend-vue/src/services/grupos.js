import api from "../services/api";

export default {
  async listar() {
    return await api.get("grupos");
  },

  async salvar(grupo) {
    return await api.post("grupos", grupo);
  },

  async alterar(grupo) {
    var novoGrupo = {
      nome: grupo.nome
    };

    return await api.put("grupos/" + grupo.id, novoGrupo);
  },

  async excluir(grupo) {
    return await api.delete("grupos/" + grupo.id);
  }
};
