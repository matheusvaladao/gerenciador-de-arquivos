import api from "../services/api";

export default {
  async listar() {
    return await api.get("usuarios");
  },

  async salvar(usuario) {
    return await api.post("usuarios", usuario);
  },

  async alterar(usuario) {
    var novoUsuario = {
      nome: usuario.nome,
      email: usuario.email,
      grupo: usuario.grupo
    };

    return await api.put("usuarios/" + usuario.id, novoUsuario);
  },

  async excluir(usuario) {
    return await api.delete("usuarios/" + usuario.id);
  }
};
