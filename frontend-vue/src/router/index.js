import Vue from "vue";
import VueRouter from "vue-router";
import DashBoard from "../components/DashBoard";
import Grupos from "../components/Grupos";
import Usuarios from "../components/Usuarios";
import Arquivos from "../components/Arquivos";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "DashBoard",
    component: DashBoard
  },
  {
    path: "/grupos",
    name: "Grupos",
    component: Grupos
  },
  {
    path: "/usuarios",
    name: "Usuários",
    component: Usuarios
  },
  {
    path: "/arquivos",
    name: "Arquivos",
    component: Arquivos
  }
];

const router = new VueRouter({
  mode: "history",
  routes
});

export default router;
